package models;

import java.time.LocalDateTime;

public abstract class Product {
	
	private String name ;
	private String brend ;
	private Double price ;
	private String color ;
	
	
	public Product() {
		
	}
	
	public Product(String name, String brend, Double price, String color ) {
		super();
		this.name = name;
		this.brend = brend;
		this.price = price;
		this.color = color;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBrend() {
		return brend;
	}
	public void setBrend(String brend) {
		this.brend = brend;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	
	public abstract double discount(LocalDateTime date);
		
}
