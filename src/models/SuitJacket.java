package models;

import java.time.LocalDateTime;

import clothingStore.BadSizeException;

public class SuitJacket extends Product {
	
	
	private final int size;

	public SuitJacket(String name, String brend, Double price, String color , int size ) {
		super(name, brend, price, color);
		verifySize(size);
		this.size = size;
	}

	public int getSize() {
		return size;
	}
	
	@Override
	public double discount(LocalDateTime date) {
		 return 0.0;
	}
	
	public void verifySize(int size) {
		if((size<42 || size>66) || (size%2==1)) {
			throw new BadSizeException("Size of suit jacket must be in range of (42-66) and must be even number.");
		}
		
	}
	
}

