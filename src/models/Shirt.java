package models;

import java.time.DayOfWeek;
import java.time.LocalDateTime;

public class Shirt extends Product {
	
	public enum ShirtSize{
		XXL,XL,L,M,S,XS 
	}
	
	
	private final ShirtSize size;
	
	public Shirt(String name, String brend, Double price, String color , ShirtSize size ) {
		super(name, brend, price, color);
		this.size = size;
	}

	public ShirtSize getSize() {
		return size;
	}
	
	public void add() {
		
	}

	@Override
	public double discount(LocalDateTime date) {
		 if(date.getDayOfWeek() == DayOfWeek.TUESDAY) {
			 return 10.0;
		 }
		 return 0.0;
	}
	

}
	
	
	
