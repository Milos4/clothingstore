package models;

import java.time.DayOfWeek;
import java.time.LocalDateTime;

import clothingStore.BadSizeException;

public class Shoes extends Product {

	
	private final int size;
	
	public Shoes(String name, String brend, Double price, String color , int size ) {
		super(name, brend, price, color);
		verifySize(size);
		this.size = size;
	}

	public int getSize() {
		return size;
	}

	@Override
	public double discount(LocalDateTime date) {
		 if(date.getDayOfWeek() == DayOfWeek.TUESDAY) {
			 return 25.0;
		 }
		 return 0.0;
	}
	
	public void verifySize(int size) {
		if(size<39 || size>46) {
			throw new BadSizeException("Size of shoes must be in range of (39-46)");
		}
		
	}
	
	
}

