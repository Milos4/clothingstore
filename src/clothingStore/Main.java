package clothingStore;

import java.time.LocalDateTime;
import java.util.List;

import models.Product;
import models.Shirt;
import models.Shirt.ShirtSize;
import models.Shoes;
import models.SuitJacket;
import models.Trousers;

public class Main {

	public static void main(String[] args) {
		Product shirt = new Shirt("Shirt" , "Nike", 100.0 , "Red", ShirtSize.XL);
		Product trousers = new Trousers("Trousers" , "Levis", 300.0 , "Blue", 48 );
		Product shoes = new Shoes("Shoes AF1" , "Nike", 220.0 , "White" , 44);
		Product suitJacket = new SuitJacket("Suit Jacket" , "Brand1", 500.0, "Blue" , 54);
		
		List<Product> products  = List.of(shoes,shirt,trousers,suitJacket);
		
		List<Product> cart  = List.of(products.get(0),products.get(1),products.get(2),products.get(3));
		
		LocalDateTime nowDate = LocalDateTime.now();
		 		
		Cashier cashier = new Cashier();
		cashier.printReceipt(cart, nowDate);
		
		
	}
	
}
