package clothingStore;

import models.Product;

public class DiscountCalculator {
	
	public static double discountCalculator(Product product,double discount) {
		double discount1 = (product.getPrice() * discount)/100;
	    return discount1;
	}
	
}
