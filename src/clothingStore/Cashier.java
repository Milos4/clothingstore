package clothingStore;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import models.Product;

public class Cashier {
	
	 DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
	 public void printReceipt(List<Product> cart, LocalDateTime now) {
		 double subTotal = 0.0;
		 double discountTotal = 0.0; 
		 
		 System.out.println("Date: " + dtf.format(now));
		 System.out.println("--Products--"+"\n");
		 
		 final double cartSizeDiscount = cart.size() >= 3 ? 20.0 : 0.0 ;
				 
		 for(final Product product : cart) {
			 
			 final double discount =  product.discount(now);
			 
			 subTotal += product.getPrice();
			 
			 discountTotal += DiscountCalculator.discountCalculator(product , Math.max(discount,cartSizeDiscount));
			 
	         System.out.println(product.getName() + " - " + product.getBrend());
	         System.out.println("$"+product.getPrice());
	         System.out.println("#discount "+  Math.round(Math.max(discount,cartSizeDiscount)) +"%-$"+ DiscountCalculator.discountCalculator(product , Math.max(discount,cartSizeDiscount)) +  "\n");
		 }
		 
		 final double total = subTotal - discountTotal; 
		 
		 System.out.println("----------------------------");
		 System.out.println("SUBTOTAL: $" + subTotal);
		 System.out.println("DISCOUNT:-$" + discountTotal);
		 System.out.println("TOTAL: $"+total);
		 
	 }
}
